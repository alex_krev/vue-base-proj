
module.exports = {
    'App render e2e tests': browser => {
        browser
            .url(`http://localhost:${8288}/#/`)
            .assert.elementPresent('#vue-app-content', 10000)
            .assert.containsText('.app-content-wrapper', 'app content')
            .end();
    }
}
