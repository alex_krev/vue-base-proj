
const webpackConfiguration = require('./../../webpack/webpack.test.js');
const configure = (karmaConfig) => {
    karmaConfig.set({
        /**
         * Список браузеров для запуска тестов
         */
        browsers: ['PhantomJS'],
        /**
         * Unit-testing frameworks
         */
        frameworks: ['mocha', 'sinon-chai', 'phantomjs-shim'],
        /**
         * Test reporters
         */
        reporters: ['spec', 'coverage'],
        /**
         * Test files
         */
        files: ['./karma-unit-spec.js'],
        /**
         * Preproc
         */
        preprocessors: {
            './karma-unit-spec.js': ['webpack', 'sourcemap', 'coverage']
        },
        /**
         * Тестовый конфиг вебпака
         */
        webpack: webpackConfiguration,
        /**
         * Чтобы вебпак не спамил в консоль лишние логи 
         */
        webpackMiddleware: {
            noInfo: true
        },
        /**
         * Генератор отчетов о покрытии и summary проекта
         */
        coverageReporter: {
            dir: './coverage',
            reporters: [
                { type: 'lcov', subdir: '.' },
                { type: 'text-summary' }
            ]
        }
    });
};

module.exports = configure;
