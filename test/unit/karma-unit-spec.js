import 'babel-polyfill';

const testsContext = require.context('./../../src/app', true, /\.spec\.js$/);
testsContext.keys().forEach(testsContext);

const srcContext = require.context('./../../src/app', true, /^\.\/(?!app(\.js)?$)/);
srcContext.keys().forEach(srcContext);

