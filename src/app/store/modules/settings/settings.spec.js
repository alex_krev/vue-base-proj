import { settings } from './settings';

describe('Settings module testing', () => {
    it('Set mutations should correctly set value to state', () => {
        const state = {
            field: 'default'
        };
        settings.mutations.set(state, {
            id: 'field',
            value: 'new value'
        });
        expect(state.field).to.equal('new value');
    });
});