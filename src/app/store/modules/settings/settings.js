
const fakeFetch = () => new Promise((res, rej) => {
    setTimeout(() => {
        res([
            { id: 1, name: 'name1' },
            { id: 2, name: 'name2' },
            { id: 3, name: 'name3' }
        ]);
    }, 3000);
});

export const settings = {
    namespaced: true
};

settings.state = {
    someField: 'test32',
    someOtherField: 'test2',
    listData: []
};

settings.mutations = {
    set: (state, {id, value}) => state[id] = value,
    merge: (state, payload) => Utils.merge(state, payload)
};

settings.actions = {
    fetchData: async ({commit}, payload) => {
        let response;
        try {
            response = await fakeFetch();
            commit('merge', { listData: response });
        } catch (err) { }
    }
};

settings.getters = {
    // some getters
};