import Vuex from 'vuex';
Vue.use(Vuex);

// модули
import { settings } from './modules/settings/settings';

/**
 * Основной модуль стора, по факту сюда просто подключаются остальные модули
 */
const store = new Vuex.Store({
    modules: {
        settings
    },
    strict: __DEV__
});

if (module.hot) {
    module.hot.accept('./modules/settings/settings.js', () => {
        store.hotUpdate({
            modules: {
                settings: require('./modules/settings/settings').settings
            }
        });
    });
}

export default store;