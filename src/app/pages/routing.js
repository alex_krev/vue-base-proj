import Router from 'vue-router';
Vue.use(Router);

// components

import MainPage from './main/MainPage.vue';

const routes = [
    {
        component: MainPage,
        name: 'main',
        path: '/'
    }
];

const router = new Router({
    mode: __DEV__ ? 'hash' : 'history',
    routes
});

export default router;