module.exports = {
    /**
     * Массив компонентов валидаторов
     */
    listValidator: [],
    /**
     * Глобальная валидация, можно передать название группы чтобы вызывать валидацию только у тех компонентов в которые был передан одноименный проп.
     * 
     * @param {string} [group=''] 
     * @returns true - если все компоненты вернули true на проверку
     */
    validate(group = '') {
        return this.listValidator
            .filter(validator => group ? validator.group === group : true)
            .every(validator => validator.validate());
    },
    /**
     * Serialize params 
     * @param {any} target 
     * @returns 
     */
    serialize(target) {
        let listURIcomponent = [];
        Object.keys(target).map(key => {
            if (key in target) listURIcomponent.push(encodeURIComponent(key) + '=' + encodeURIComponent(target[key]));
        });
        return listURIcomponent.join('&');
    },

    /**
     * Общая функция для отправки запросов к api  
     * Формат обработки заточен под стандартизированную форму респонса принятую у нас на BE
     * @param {string} url 
     * @param {any} [params={}] 
     * @returns 
     */
    ajax(url, params = {}) {
        return new Promise((resolve, reject) => {
            try {
                let xhr = new (XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
                xhr.open('POST', apiPath + url, true);
                xhr.withCredentials = true;
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                xhr.onreadystatechange = () => {
                    if (xhr.readyState > 3) {
                        let response = '';
                        try {
                            response = JSON.parse(xhr.responseText);
                            response.isOk ? resolve(response) : reject(response);
                        } catch (err) {
                            reject({
                                isOk: false,
                                message: xhr.responseText
                            });
                        }
                    }
                };
                xhr.send(this.serialize(params));
            } catch (err) {
                reject({
                    isOk: false,
                    message: err
                });
            }
        });
    },

    merge: (target, payload) => Object.keys(payload).map(key => Vue.set(target, key, payload[key]))

};