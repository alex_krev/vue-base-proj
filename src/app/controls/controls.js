const controls = {
    'base-input': require('./baseInput/BaseInput'),
    'validator': require('./validator/Validator.vue')
};

/**
 * Глобально объявляем все контролы 
 */
Object.keys(controls).map(key => Vue.component(key, controls[key]));