# Naming convention

Общие правила которые нужны для единообразного именования переменных, функций, классов, файлов, папок и т.д


Общее:
- lowerCamaelCase 
- Наименования в единственном числе
- Никакого транслита

| Кейс | Хорошо | Плохо |    
|---|---|---|
| Название папок и файлов|Название в lowerCamelCase, исключение: Файлы .vue - UpperCamelCase | App.js, some-file.js, some-component.vue, some_file.js |


#### CSS, SCSS
| Кейс | Хорошо | Плохо |    
|---|---|---|
|Основной модуль scss|main.scss, в который импортируются остальные scss модули|Название отличное от main.scss, стилизация в самом main.scss|
|Модуль scss со стилизацией|_styleModuleName.scss|some-styledModuleName.scss|
|Внешние модули scss|Папка vendor в папке со стилями в которой находится модуль vendor.scss, в который импортируются другие модули находящиеся рядом в этой же папке, затем модуль vendor.scss импортируется в main.scss|Внешние модули импортируются в main.scss, внешние модули находятся вне папки vendor, собирательный файл внешних модулей не vendor.scss|
|Именование классов в css/scss|Использование BEM методологии|Хаотичное именование классов|

#### JS
| Кейс | Хорошо | Плохо |    
|---|---|---|
|Название .vue компонентов|UpperCamelCase.vue|some-randomNaming-strategy.vue|
|Название функций, методов|lowerCamelCase( )|do_staff( )|
|Название методов класса .vue компонентов, которые вызываются для обработки событий| {on}{OptionalDomainName}{EventName}: onSomeStaffClick, onChange, onSubmit, onBtnClick | btnClick, formSubmit, submit, click |
|Переменные|lowerCamelCase: someLongVariableName, someOtherName | some_variable_name |
|Название переменных содержащих массив| Префикс "list", окончание в единственном числе. Пример: listNumber, listSomeStaff, listUser. ||
|Boolean переменные| Префкис "is". isDisabled, isOk, isGoodPractice | disabled, ok, goodPractice, selected |